# BDD I-garden 
Au sein d'un [autre exercice](https://gitlab.com/docusland-courses/database/bdd-i-garden) nous implémentons une base de données liée aux plantes. 

Au sein de ce dit exercice, il est attendu de produire des vues, peut-être des triggers. Mais pour la plupart du temps, les apprenants n'ont pas le temps de réaliser de procédure stockées.

Ainsi, dans le même contexte, nous allons donc reprendre ces outils forts pratiques, les procédures stockées et les triggers.

Pour ce faire, afin de tous partir d'une structure semblable importez le [fichier i-garden.sql](./i-garden.sql) présent au sein de ce dépot au sein de votre BDD mysql ou mariadb.

Puis suivez les consignes suivantes 

## Triggers

Une plante possède des états, historisez les anciens états et statuts des plantes au sein d'une nouvelle table "Historisation_Etat".
Attention, il est important d'historiser la date également.

## Procédure stockée

A partir du fichier SQL suivant [link](./i-garden.sql), écrire deux procédure stockées. 

```sql
CREATE PROCEDURE generate_next_year_task(IN id_task INT)
BEGIN
  -- your code
END //
CREATE PROCEDURE generate_next_year_tasks()
BEGIN
  -- your code
END //
```

La procédure stockée `generate_next_year_tasks` devra parcourir l'ensemble des tâches présentes dans la base de données, et appeler `generate_next_year_task`.  Attention, elle ne devra dupliquer que les taches, générées dans l'année précédant l'execution de la procédure.
Par exemple, si elle est executée en 2023, elle ne doit dupliquer que les taches de 2022.  
Elle devra également ignorer les taches n'ayant pas été réalisées l'année précédente (ayant un statut `ignored`).

La procédure `generate_next_year_task` dupliquera, à partir d'un ID donné, la tâche pour l'année prochaine.
Si la tache demandée est trop ancienne ou trop récente, une exception doit être levée.

Attention, votre procédure stockée devra s'assurer que les utilisateurs finaux ne puissent modifier les données de la table tasks pendant l'exécution de la procédure `generate_next_year_tasks`. 

Egalement, en cas d'erreur, une exception devra être émise.