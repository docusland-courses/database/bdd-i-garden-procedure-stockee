
START TRANSACTION;
DROP DATABASE IF EXISTS `i-garden`;
CREATE DATABASE IF NOT EXISTS `i-garden` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;
USE `i-garden`;

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `Plant` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `latin_name` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

INSERT INTO `Plant` (`id`, `name`, `latin_name`) VALUES
(1, 'Pommier Chantecler', 'Malus domestica Borkh \'Chantecler\''),
(2, 'Lierre grimpant', 'Hedera helix L.'),
(3, 'courge spaghetti', 'Cucurbita pepo');

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `Plant_has_Task` (
  `plant_id` int NOT NULL,
  `task_id` int NOT NULL,
  `status` enum('To do','Done','Ignored','') NOT NULL DEFAULT 'To do',
  PRIMARY KEY (`plant_id`,`task_id`),
  KEY `task_id` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

INSERT INTO `Plant_has_Task` (`plant_id`, `task_id`, `status`) VALUES
(1, 1, 'Done'),
(1, 2, 'To do'),
(1, 3, 'Done'),
(3, 4, 'Done'),
(3, 6, 'Ignored');

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `Task` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `date_beg` date NOT NULL,
  `date_end` date NOT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

INSERT INTO `Task` (`id`, `name`, `date_beg`, `date_end`, `description`) VALUES
(1, 'Taille du pommier', '2021-11-01', '2021-12-30', 'En premier lieu, munissez-vous d\'un bon sécateur bien affûté afin de ne pas \"mâcher\" la coupe qui favoriserait le développement de maladies. Sachez aussi que la coupe doit être orientée dans le même sens que le bourgeon sous-jacent.\r\n\r\nAvant l\'hiver, en ce qui concerne les tiges et les demi-tiges, votre taille à trois yeux aérera le centre de l\'arbre. Vous rajeunirez les charpentières uniquement tous les 6 ou 7 ans, à deux yeux. Quant aux rameaux les plus vigoureux, vous les taillerez à quatre yeux. '),
(2, 'Pincez les rameaux pour alléger les branches', '2023-06-04', '2023-07-30', 'Dès le mois de juin, vous pourrez pincer les rameaux à bois, ce qui allègera l\'arbre. Par ailleurs, si vous constatez trop de bourgeons à fruits, vous pourrez les éclaircir afin de ne laisser qu\'un fruit par bouquet.'),
(3, 'Taille du pommier', '2022-11-01', '2022-12-30', 'En premier lieu, munissez-vous d\'un bon sécateur bien affûté afin de ne pas \"mâcher\" la coupe qui favoriserait le développement de maladies. Sachez aussi que la coupe doit être orientée dans le même sens que le bourgeon sous-jacent.\r\n\r\nAvant l\'hiver, en ce qui concerne les tiges et les demi-tiges, votre taille à trois yeux aérera le centre de l\'arbre. Vous rajeunirez les charpentières uniquement tous les 6 ou 7 ans, à deux yeux. Quant aux rameaux les plus vigoureux, vous les taillerez à quatre yeux. '),
(4, 'Semis en place de la courge', '2022-05-31', '2022-08-01', 'La courge redoute le froid et a donc besoin d’un climat tempéré à chaud pour lever correctement.\r\n\r\nElle aime les sols plutôt riches, n’hésitez pas à amender le sol de compost ou d’un fertilisant avant de la cultiver.\r\n\r\n'),
(6, 'Semis sous abri de la courge', '2022-04-23', '2022-05-31', 'La courge spaghetti redoute le froid et a donc besoin d’un climat tempéré à chaud pour lever correctement.\r\n\r\nLe semis en godet doit se faire environ 3 semaines avant le repiquage en pleine terre. Il ne faut donc pas semer trop tôt.\r\n\r\n    Enfoncer légèrement 2 à 3 graines par godet.\r\n    Assurez vous que la température ne descende pas sous les 12° pendant la germination.\r\n    Une fois levée, on ne garde que la plante la plus vigoureuse.\r\n    3 semaines après, on peut mettre en terre à condition que tout risque de gelée soit écarté.\r\n    Prévoir un espace de 1 m minimum entre chaque pied.\r\n');

ALTER TABLE `Plant_has_Task`
  ADD CONSTRAINT `Plant_has_Task_ibfk_1` FOREIGN KEY (`plant_id`) REFERENCES `Plant` (`id`),
  ADD CONSTRAINT `Plant_has_Task_ibfk_2` FOREIGN KEY (`task_id`) REFERENCES `Task` (`id`);


CREATE TABLE IF NOT EXISTS `State` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

INSERT INTO `State` (`name`) VALUES ( 'Malade'), ('Assoiffé'), ('Carrence de potasse'), ('Carrence de fer');


CREATE TABLE IF NOT EXISTS `Plant_has_State` (
  `id` int NOT NULL AUTO_INCREMENT,
  `plant_id` int NOT NULL,
  `state_id` int NOT NULL,
  `date_detection`  TIMESTAMP NOT NULL DEFAULT  CURRENT_TIMESTAMP,
  `status` enum('Actif','Traité','A vérifier','Soigné') NOT NULL DEFAULT 'Actif',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

ALTER TABLE `Plant_has_State`
  ADD CONSTRAINT `Plant_has_State_ibfk_1` FOREIGN KEY (`plant_id`) REFERENCES `Plant` (`id`),
  ADD CONSTRAINT `Plant_has_State_ibfk_2` FOREIGN KEY (`state_id`) REFERENCES `State` (`id`);

INSERT INTO `Plant_has_State` (`plant_id`, `state_id`, date_detection) VALUES ( 1, 1, '2023-06-10'),( 3, 2,'2023-06-10');

INSERT INTO `Plant_has_State` (`plant_id`, `state_id`) VALUES (1,4);

COMMIT;